#!/usr/bin/python
# encoding: utf-8


#==================================
#@file name: game
#@author: Lixin Zou
#@contact: zoulixin15@gmail.com
#@time:9/11/18,9:26 AM
#==================================

import numpy as np
import ipdb




class game(object):
    def __init__(self):
        pass

    def start_game(self):
        ## return start state, reward, terminate.
        raise NotImplementedError

    def do_action(self,action):
        # return next state, reward, terminate.
        raise NotImplementedError

    def end_or_not(self):
        # return the game end or not.
        raise NotImplementedError


